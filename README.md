# Yet-Another-Tower-Defence
Tower Defence game demo with:
  + 3 enemy types
  + 3 tower types, each designed to work well in concrete situations:
    1. Simply Good DPS and fire rate
    2. Anti armor, good for focusing someone with high HP
    3. AOE - killing groups of enemies
  + Armor mechanic
  + Integrated level editor - can change every aspect of game in the menu
---

<img src="/screens/arrows.png" width="850" height="500" />
<img src="/screens/fireball.png" width="850" height="500" />
<img src="/screens/catapult.png" width="950" height="450" />
<img src="/screens/catapult_2.png" width="850" height="500" />
<img src="/screens/wave_editor.png" width="665" height="522" />
 

---
Get the built version here:
  [Google Drive](https://drive.google.com/drive/folders/0B20aJPTAEBHmUzZXTVA5NUgwQ2s)
